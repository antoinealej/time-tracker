export const getDateFromString = (stringDate) => {
  const [day, month, year] = stringDate.split('-');
  const date = new Date();
  date.setDate(day);
  date.setMonth(month - 1);
  date.setFullYear(year);
  return date;
}
export const getDateToString = (date) => `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
export const getTodayDate = () => getDateToString(new Date());
export const getDateTime = (date) => `${('0' + date.getHours()).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}:${('0' + date.getSeconds()).slice(-2)}`;
export const utcMsTimestampToTime = (ms) => getDateTime(new Date(ms));
export const getTimeDiff = (date1, date2) => Math.round((date1 - date2) / 1000);
export const roundedDuration = ms => {
  const s = parseFloat((ms / 1000).toFixed(0));
  return (~~(s/3600) + ~~(s%3600/3600/.25)*.25).toFixed(2);
}
export const parseTime = (timeString) => {
  const d = new Date();
  const [h, m, s] = timeString.split(':');
  d.setHours(h);
  d.setMinutes(m);
  d.setSeconds(s);
  return d;
}
