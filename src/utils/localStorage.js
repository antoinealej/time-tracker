export const getValue = (key, jsonType = null) => {
  const raw = localStorage.getItem(key);
  let result = raw;
  try {
    result = JSON.parse(raw)
    if (jsonType && !result) {
      switch (jsonType) {
        case 'array': return [];
        case 'object': return {};
        default: return {};
      }
    }
  } catch (e) {
    if (jsonType) {
      switch (jsonType) {
        case 'array': return [];
        case 'object': return {};
        default: return {};
      }
    }
    console.log('not json')
  }
  return result;
}

export const setValue = (key, value) => {
  let result;
  try {
    result = JSON.stringify(value);
  } catch (e) {
    result = value;
  }
  localStorage.setItem(key, result);
}
