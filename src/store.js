import Vue from 'vue'
import Vuex from 'vuex'
import { getTodayDate, getDateFromString, getDateToString } from '@/utils/date';
import { getValue, setValue } from '@/utils/localStorage';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    running: (localStorage.getItem('running') === 'true') && getValue(getTodayDate(), 'array').length,
    selectedDate: getTodayDate(),
    dayDataHash: ''
  },
  getters: {
    getDayData (state) {
      return getValue(state.selectedDate, 'array', state.dayDataHash)
    },
  },
  mutations: {
    changeDay (state, count) {
      const date = getDateFromString(state.selectedDate);
      date.setDate(date.getDate() + count);
      state.selectedDate = getDateToString(date)
    },
    start (state) {
      state.running = true
      localStorage.setItem('running', 'true');
      const data = getValue(getTodayDate(), 'array');
      data.push({ start: Date.now() });
      setValue(getTodayDate(), data);
      state.dayDataHash = JSON.stringify(data)
    },
    stop (state) {
      state.running = false
      localStorage.setItem('running', 'false');
      const data = JSON.parse(localStorage.getItem(getTodayDate()) || '[]') || [];
      data[data.length - 1].stop = Date.now();
      setValue(getTodayDate(), data);
      state.dayDataHash = JSON.stringify(data)
    }
  }
})

export default store;
